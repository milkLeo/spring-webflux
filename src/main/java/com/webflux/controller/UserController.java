package com.webflux.controller;

import com.webflux.model.UserDO;
import com.webflux.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.IOException;

@Controller
@RequestMapping("/user")
public class UserController {


    @Autowired
    private UserService userService;

    @RequestMapping(value = "/create",method = RequestMethod.POST)
    public Mono<UserDO>insert(UserDO userDO) throws IOException {
        return userService.save(userDO);
    }

    @RequestMapping(value = "/findAll",method = RequestMethod.GET)
    public Flux<UserDO> queryAll() throws IOException {
        return userService.findAll();
    }


    @RequestMapping(value = "/queryPage",method =RequestMethod.GET)
    public Flux<UserDO> queryPage() throws IOException {
        return userService.findAll();
    }

}
