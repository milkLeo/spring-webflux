package com.webflux.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

import java.util.Date;
@Data
@Table("t_user")
public class UserDO {

    @Id
    private Integer id;

    private String phone;

    private String password;

    @Column("create_time")
    private Long createTime;

    private String username;

    private String alias;


}
