package com.webflux.handler;

import com.webflux.dto.PageDO;
import com.webflux.model.UserDO;
import com.webflux.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import java.io.IOException;
import java.util.Date;

@Component
public class UserHandler {

    @Autowired
    private UserService userService;


    public Mono<ServerResponse> insert(ServerRequest serverRequest) throws IOException {


        UserDO userDO=new UserDO();
        userDO.setPhone("123");
        userDO.setPassword("123");
        userDO.setCreateTime(new Date().getTime());
        userDO.setUsername("123");
        return ServerResponse.ok()
                .body(userService.save(userDO).then(),UserDO.class);

    }


    public Mono<ServerResponse> findById(ServerRequest serverRequest) {
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(userService.findById(1),UserDO.class);

    }


    public Mono<ServerResponse> findAll(ServerRequest serverRequest) throws IOException {
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_JSON)
                .body(userService.findAll(),UserDO.class);

    }

}
