package com.webflux.entity;


import lombok.Data;

@Data
public class ElasticSearchEntity <T>{

    private T object;

    public ElasticSearchEntity(T object) {
        this.object = object;
    }
}
