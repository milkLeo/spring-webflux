package com.webflux.entity;

import com.webflux.model.UserDO;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

import java.util.Date;

@Data
@Document(indexName = "t_user",type = "docs", shards = 1, replicas = 0)
public class UserEntity {

    @Id
    @Field(type = FieldType.Keyword)
    private Integer id;
    private String phone;

    @Field(type = FieldType.Text, analyzer = "ik_max_word", searchAnalyzer = "ik_smart")
    private String username;
    @Field(type = FieldType.Text, analyzer = "ik_max_word", searchAnalyzer = "ik_smart")
    private String alias;

    private String password;

    private Date createTime;


    public UserEntity() {

    }


    public UserEntity(UserDO userDO) {
        this.id = userDO.getId();
        this.phone=userDO.getPhone();
        this.password=userDO.getPassword();
        this.username=userDO.getUsername();
        this.createTime=new Date(userDO.getCreateTime());
        this.alias=userDO.getAlias();
    }
}
